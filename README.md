# Web Development Assignment
---

## Problem Statement
---
Build a Trello clone with the following tech stack

- Postgres
- Angular
- Node
- Express
- Docker

The clone will be evaluated for the following features

- login workflow without social auth
- creating and deleting multiple boards
- creating, removing and updating lists in each board
- creating, removing and updating cards in each board
- ability to drag and change order of lists
- ability to be able to drag cards across lists
- horizontal and vertical scroll on each board

You will be evaluated on the following things

- number of the above features implemented
- your ability to replicate designs as closely as possible
- quality of code
- speed of the application
- code documentation

## Submission
---
Create a public github repository on your personal handle and push all the code on that repository. Add a README which explains how to run the code Also explain the approach that you took, the features you implemented, the ones you missed, and improvements you can make to make your code more efficient. Submit the link to the repository to your point of contact. 